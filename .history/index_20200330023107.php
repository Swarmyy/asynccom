<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <script>
        //ex1
        var musicStyles = [];
        $.getJSON('ex1.php', musicStyles,
            function(response) {
                var len = response.length;
                for (var i = 0; i < len; i++) {
                    musicStyles.push(response[i]);
                }
            }
        );

        $(function() {
            $("#autocomplete").autocomplete({
                source: musicStyles,
                autoFocus: true
            });
        });

        //ex2
        var groups = [];

        $.getJSON('ex2.php', groups, function(response) {
            var len = response.length;
            for (var i = 0; i < len; i++) {
                groups.push(response[i]);
            }
        })

        /*
        $.ajax({
            url: 'ex2.php',
            type: 'get',
            dataType: 'JSON',
            success: function(response) {
                var len = response.length;
                for (var i = 0; i < len; i++) {
                    groups.push(response[i]);
                }
            }
        });
        */

        $("document").ready(function() {
            var _GET = <?php echo json_encode($_GET) ?>;
            console.log(_GET);
            console.log(groups);
            console.log(groups.includes(_GET["group"]));
            $("#isgroup").text(groups.includes(_GET['group']));
        })

        $('#isgr'){
            
        }
    </script>
</head>

<body>
    <h1>EX1</h1>
    <label for='autocomplete'>Style Musical:</label>
    <input id="autocomplete">

    <h1>EX2</h1>
    <div>Passez en paramètre le group dont vous voulez savoir l'existence, par exemple : </div>
    <div>?group=SuperRock</div>
    <div>
        <span>Le groupe passé en paramètre existe-t-il ? </span>
        <span id="isgroup"></span>
    </div>
    <div>
        <label for='isgr'>Ce groupe existe-t-il?</label>
        <input id='isgr'>
    </div>

</body </html>