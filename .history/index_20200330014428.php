<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <script>
        function ajaxQuery(type, url, dataType) {
            jQuery.ajax({
                type: type,
                url: url,
                dataType: dataType,
                data: {
                    test: 'add',
                    arguments: []
                },
                success: function(obj, textstatus) {
                    if (!('error') in obj) {
                        return obj.result;
                    } else {
                        console.log(obj.error);
                    }
                }
            });
        }

        //ex1
        var musicStyles=[];
        $.ajax({
            url: 'ex1.php',
            type: 'get',
            dataType: 'JSON',
            success: function(response) {
                var len = response.length;
                for (var i = 0; i < len; i++) {
                    musicStyles.push(response[i]);
                }
            }
        });

        $(function() {
            $("#autocomplete").autocomplete({
                source: musicStyles,
                autoFocus: true
            });
        });


        var _GET = <?php echo json_encode($_GET) ?>;
        $(function() {
            $("#autoGET").autocomplete({
                source: _GET,
                autoFocus: true
            });
        });

        console.log(_GET);
    </script>
</head>

<body>
    <h1>EX1</h1>
    <label for='autocomplete'>Style Musical:</label>
    <input id="autocomplete">

    <h1>EX2</h1>
    <div></div>

</body </html>